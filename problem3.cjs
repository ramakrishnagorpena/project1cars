function carModelsList(inventory){
    if(inventory.length === 0){
        return inventory;
    }
   let models = [];
   if(Array.isArray(inventory)){
        for(let index = 0; index < inventory.length; index++){
            models.push(inventory[index].car_model);
        }
    }
    else{
        return [];
    }
    models.sort();
    return models;
}

module.exports=carModelsList;