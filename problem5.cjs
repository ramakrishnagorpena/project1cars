function olderCarCountDetails(inventory){
    if(inventory.length === 0){
        return inventory;
    }
    let year=[];
    if(Array.isArray(inventory)){
        for (let index = 0; index < inventory.length; index++) {
            year.push(inventory[index].car_year);
        }
    }
    else{
        return [];
    }
    year.sort();
    let count=0;
    for (let index = 0; index < year.length; index++) {
        if(year[index] < 2000){
           count++; 
        }
    }
    return count;
}

module.exports = olderCarCountDetails;