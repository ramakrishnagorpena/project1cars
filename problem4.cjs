function carYearData(inventory){
    if(inventory.length === 0){
        return inventory;
    }
    let year=[];
    if(Array.isArray(inventory)){
        for (let index = 0; index < inventory.length; index++) {
            year.push(inventory[index].car_year);
        }
    }
    else{
        return [];
    }   
    return year;
}

module.exports = carYearData;