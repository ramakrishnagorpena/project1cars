function lastCarInfo(inventory){
    if(inventory.length === 0){
        return inventory;
    }
    if(Array.isArray(inventory)){
        for (let index = inventory.length-1 ; index > 0 ; index--) {
            return ("Last car is a "+inventory[index].car_make+" "+inventory[index].car_model);
            break;
        }
    }
    else{
        return [];
    }
}

module.exports = lastCarInfo;