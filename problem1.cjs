function car33Info(inventory=[], id){
    if(inventory.length === 0){
        return inventory=[];
    }
    if(id===null || typeof(id)!='number'){
        return id=[];
    }
    if(Array.isArray(inventory)){
        for (let index = 0;index < inventory.length; index++){
            if(inventory[index].id === id){
                return inventory[index];
            }
        }
    }
    else{
        return inventory=[];
    }
}

module.exports = car33Info;
