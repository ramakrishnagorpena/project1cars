function bmwAndAudiData(inventory){
    if(inventory.length === 0){
        return inventory;
    }
    let make=[];
    if(Array.isArray(inventory)){
        for (let index = 0; index < inventory.length; index++) {
            if(inventory[index].car_make=="BMW"||inventory[index].car_make=="Audi")
            make.push(inventory[index]);
        }
    }
    else{
        return [];
    }
    return make;
}

module.exports = bmwAndAudiData;